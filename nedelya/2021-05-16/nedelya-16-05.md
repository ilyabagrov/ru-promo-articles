# На этой неделе в KDE: бета Plasma 5.22 уже здесь!

> 9–16 мая, основное — прим. переводчика

На этой неделе мы закончили работу над новыми функциями в Plasma 5.22, так что [можете опробовать бета-выпуск](https://kde.org/announcements/plasma/5/5.21.90/)! Мы также приступили к работе над 5.23, исправили пачку ошибок в Wayland и ещё немного отполировали наши приложения. Посмотрите сами:

## Новые возможности

Виджет цифровых часов [получил возможность отображать часовые пояса не как их коды или названия городов, а как смещения относительно времени UTC](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/751) (Momo Cao, Plasma 5.22):

![0](https://pointieststick.files.wordpress.com/2021/05/utc-time-offset.png?w=1024)

## Исправления и улучшения производительности

Gwenview [теперь использует стандартный графический компонент из Qt для показа изображений](https://invent.kde.org/graphics/gwenview/-/merge_requests/66), что [исправляет распространённый сбой отображения при панорамировании и масштабировании](https://bugs.kde.org/show_bug.cgi?id=271671), уменьшает потребление памяти и улучшает отзывчивость, особенно при масштабировании щипком (Arjen Hiemstra, Gwenview 21.08)

Исправлено [наиболее распространённое зацикливание привязки в приложениях на базе QtQuick](https://bugs.kde.org/show_bug.cgi?id=435530) (что вызывало спам в логах, а также снижение производительности) (Arjen Hiemstra, Frameworks 5.83)

При запуске сеанса через Systemd вы [теперь можете разблокировать сессию, используя консольную утилиту `loginctl`](https://bugs.kde.org/show_bug.cgi?id=433364) (David Edmundson, Plasma 5.22)

## Wayland

В сеансе Plasma Wayland [служба интеграции браузеров больше не падает циклично, когда Firefox просит сделать его браузером по умолчанию](https://bugs.kde.org/show_bug.cgi?id=435811), что, кстати, происходит слишком часто [из-за ошибки в самом Firefox при установленной переменной окружения GTK\_USE\_PORTAL=1](https://bugzilla.mozilla.org/show_bug.cgi?id=1516290), как это сделано по умолчанию в Neon и Fedora (Harald Sitter, Plasma 5.22)

В сеансе Wayland [KWin больше не падает, когда внешний монитор уходит в режим ожидания или отключается](https://bugs.kde.org/show_bug.cgi?id=436095) (Влад Загородний, Plasma 5.22)

В сеансе Wayland перетаскивание приложений из панели задач на другой рабочий стол через виджет переключения рабочих столов [теперь работает](https://bugs.kde.org/show_bug.cgi?id=436823) (David Redondo, Plasma 5.22)

В сеансе Wayland кнопка меню приложения в заголовке окна [больше не открывает меню странным отдельным окном](https://bugs.kde.org/show_bug.cgi?id=430662), и [навигация по меню с клавиатуры теперь тоже работает корректно](https://bugs.kde.org/show_bug.cgi?id=420980) (David Redondo, Plasma 5.22)

## Улучшения пользовательского интерфейса

Dolphin теперь использует KHamburgerMenu, что позволило нам [сделать содержимое меню-гамбургера более актуальным, менее избыточным, не таким пугающим и лучше умещающимся на маленьких экранах](https://invent.kde.org/system/dolphin/-/merge_requests/168). Функциональность не пострадала, просто возможности были переупорядочены так, чтобы популярные были на виду, а менее популярные не лезли под руку (Felix Ernst, Dolphin 21.08):

![1](https://pointieststick.files.wordpress.com/2021/05/screenshot_20210514_225806.png?w=1024)

Что действительно круто, так это то, что меню отзывается на вашу персонализацию. К примеру, поскольку я добавил действие «Создать…» в панель инструментов, его больше нет в меню. Но я удалил с панели действие «Фильтр», и оно ушло в меню.

Виджет принтеров в системном лотке [стал проще и привычнее](https://invent.kde.org/utilities/print-manager/-/merge_requests/9): его кнопка настроек теперь открывает соответствующую страницу Параметров системы, а опции самого виджета переехали в меню-гамбургер, как это сделано в виджете «Диски и устройства» (Nate Graham, print-manager 21.08):

![3](https://pointieststick.files.wordpress.com/2021/05/screenshot_20210507_232658.png?w=1024)

Вы [теперь можете отключить пункт «Открыть терминал» в контекстном меню Dolphin](https://bugs.kde.org/show_bug.cgi?id=436323) (Alexander Lohnau, Dolphin 21.08)

Когда Dolphin отображает скрытые файлы, [они теперь расположены после видимых, а не перед ними](https://bugs.kde.org/show_bug.cgi?id=241227) (Gastón Haro, Dolphin 21.08)

Эффект «Полупрозрачность» рабочего стола [теперь по умолчанию отключён, и окна не становятся слегка прозрачными при перемещении или изменении размера](https://bugs.kde.org/show_bug.cgi?id=384054) (Nate Graham и Влад Загородний, Plasma 5.23)

Кнопка «Добавить новое устройство…» виджета Bluetooth в системном лотке [теперь находится в заголовке, в соответствии с другими виджетами системного лотка](https://invent.kde.org/plasma/bluedevil/-/merge_requests/38) (Nate Graham, Plasma 5.22):

![4](https://pointieststick.files.wordpress.com/2021/05/bluetooth-add-new-device-button-in-header.png?w=1024)

Теперь возможно [автоматически упорядочить приложения в панели задач «только значки» по различным критериям (по алфавиту, рабочему столу, и т.д), как и на обычной панели задач](https://bugs.kde.org/show_bug.cgi?id=423213) (Marko Gobin, Plasma 5.22)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [boingo-00](https://t.me/boingo00)  
_Источник:_ https://pointieststick.com/2021/05/14/this-week-in-kde-the-plasma-5-22-beta-is-here/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icons → значки -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
