# На этой неделе в KDE: как GNOME в «Обзор» поместился

> 14–21 ноября, основное — прим. переводчика

На этой неделе новый эффект диспетчера окон KWin «Обзор» получил способность [показывать результаты поиска из KRunner](https://bugs.kde.org/show_bug.cgi?id=445207)! Это заметно сближает его по набору функций с обзором рабочих столов в GNOME!

![0](https://pointieststick.files.wordpress.com/2021/11/overview-with-search-results.png)

*С такими темпами, мы примерно на полпути к реализации всей оболочки GNOME в эффекте «Обзор»*

Спасибо Владу Загороднему за проделанную работу, которая увидит свет в Plasma 5.24!

## Другие новые возможности
<!-- первое упоминание в статье, но на роль программы указывает конец предложения  -->
Gwenview [теперь имеет возможность предпросмотра печати](https://bugs.kde.org/show_bug.cgi?id=236056), что, как можно себе представить, весьма полезно для просмотрщика изображений (Александр Волков, Gwenview 22.04)

<!-- первое упоминание в статье -->
Центр приложений Discover [теперь предотвращает любые ваши действия, которые приведут к удалению Plasma в процессе](https://bugs.kde.org/show_bug.cgi?id=445226), так как это, вероятно, не то, что вы хотели сделать (Aleix Pol Gonzalez, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2021/11/cant-remove-plasma-1.png)

## Исправления ошибок

Печать изображений из [Gwenview](https://invent.kde.org/graphics/gwenview/-/merge_requests/115) или из [графического редактора KolourPaint](https://invent.kde.org/graphics/kolourpaint/-/merge_requests/11) теперь автоматически выбирает ландшафтный или портретный режим исходя из соотношения сторон печатаемого изображения, а не заставляет вас выбирать режим вручную (Александр Волков, Gwenview 21.12)

У кнопок на панели инструментов в GTK-приложениях, не использующих декорации окон на стороне клиента, таких как Inkscape и FileZilla, [больше нет ненужной обводки](https://bugs.kde.org/show_bug.cgi?id=443626) (Ярослав Сидловский, Plasma 5.23.4)

Диалоги открытия и сохранения файлов в приложениях Flatpak и Snap [теперь запоминают и восстанавливают свой размер при повторном открытии](https://bugs.kde.org/show_bug.cgi?id=443251) (Евгений Попов, Plasma 5.23.4)

Discover [теперь прекращает поиск, когда результатов больше нет, вместо того чтобы продолжать отображать «Выполняется поиск…» внизу](https://bugs.kde.org/show_bug.cgi?id=427244) (Aleix Pol Gonzalez, Plasma 5.24)

<!-- не смог понять, при чем здесь embedded -->
Исправлена [проблема с воспроизведением некоторых видео в сеансе Plasma Wayland](https://bugs.kde.org/show_bug.cgi?id=445346) (Влад Загородний, Plasma 5.24)

Множество мелких недочётов было устранено в наборе значков Breeze — слишком много, чтобы перечислить по отдельности! (Andreas Kainz, Frameworks 5.89)

В Plasma [исправлено мерцание всплывающих подсказок, когда они появляются или исчезают](https://bugs.kde.org/show_bug.cgi?id=305247) (Marco Martin, Frameworks 5.89)

Значки и текст на вкладках в виджетах Plasma [теперь снова отцентрированы, как и полагается](https://bugs.kde.org/show_bug.cgi?id=442830) (Евгений Попов, Frameworks 5.89)

## Улучшения производительности

Konsole [теперь освобождает память сразу при очистке журнала](https://invent.kde.org/utilities/konsole/-/merge_requests/513) (Martin Tobias Holmedahl Sandsmark, Konsole 22.04)

В Konsole [улучшена производительность](https://bugs.kde.org/show_bug.cgi?id=443885) [отрисовки текста](https://invent.kde.org/utilities/konsole/-/merge_requests/525) (Waqar Ahmed и Tomaz Canabrava, Konsole 22.04)

Текстовый список из сгруппированных приложений на панели задач [стал намного производительнее и быстрее](https://bugs.kde.org/show_bug.cgi?id=433907) (Fushan Wen, Plasma 5.24)

Исправлена серьёзная проблема производительности [в эффектах KWin на базе QtQuick у владельцев видеокарт NVIDIA](https://invent.kde.org/plasma/kwin/-/merge_requests/1689) (David Edmundson, Plasma 5.24)

Новый эффект «Обзор» [теперь гораздо быстрее запускается](https://bugs.kde.org/show_bug.cgi?id=445666) (Влад Загородний, Plasma 5.24)

## Улучшения пользовательского интерфейса

<!-- первое упоминание в статье -->
У музыкального плеера Elisa [теперь более приятная глазу и уместная обложка альбома по умолчанию](https://bugs.kde.org/show_bug.cgi?id=445357) (Andreas Kainz, Elisa 22.04):

![2](https://pointieststick.files.wordpress.com/2021/11/new-album-icon.png)

Новый эффект «Обзор» [теперь поддерживает сенсорный ввод](https://invent.kde.org/plasma/kwin/-/merge_requests/1663) (Arjen Hiemstra, Plasma 5.24)

Виджет «Сенсорная панель» [восстановлен после того, как был удалён из Plasma 5.23](https://bugs.kde.org/show_bug.cgi?id=445095), и теперь играет роль [пассивного индикатора состояния вкл./выкл. сенсорной панели](https://invent.kde.org/plasma/plasma-desktop/-/commit/57d5f02d9aff1c784d08b4ff865ac9f4f0af6f0e), наряду с виджетами-уведомлениями «Состояние фиксации режимов клавиш» (Caps Lock) и «Микрофон» (Nate Graham, Plasma 5.23.4):

![3](https://pointieststick.files.wordpress.com/2021/11/touchpad-disabled.png)

Discover [теперь показывает более простые и понятные сообщения при возникновении проблем в процессе установки обновлений](https://invent.kde.org/plasma/discover/-/merge_requests/203) (Nate Graham, Plasma 5.24):

* ![5](https://pointieststick.files.wordpress.com/2021/11/friendly-view.png)

* ![6](https://pointieststick.files.wordpress.com/2021/11/nerdy-view.png)

Конечно же, это поддельное сообщение о выдуманной проблеме. 🙂 Но именно так обыкновенные ошибки выглядят для рядовых пользователей!

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Иван Ткаченко (ratijas)](https://t.me/ratijas)  
_Источник:_ https://pointieststick.com/2021/11/19/this-week-in-kde-most-of-gnome-shell-in-the-overview-effect/  

<!-- 💡: applet → виджет -->
<!-- 💡: headerbar → панель заголовка -->
<!-- 💡: icon → значок -->
<!-- 💡: task manager → панель задач -->
