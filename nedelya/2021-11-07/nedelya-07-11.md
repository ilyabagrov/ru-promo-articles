# На этой неделе в KDE: стабильность, мощь и красивый багтрекер

> 31 октября – 7 ноября, основное — прим. переводчика

## Новые возможности

Skanlite [теперь имеет функцию «пакетного режима» для планшетных сканеров без автоподатчика документов. Теперь приложение автоматически выполнит новое сканирование через несколько секунд, чтобы ускорить процесс сканирования](https://bugs.kde.org/show_bug.cgi?id=411229). В [Skanpage](https://apps.kde.org/skanpage/) это также скоро появится! (Alexander Stippich, Skanlite 21.12)

## Wayland

В сеансе Wayland [выключение и последующее включение внешнего монитора больше не приводит к падению Plasma](https://bugs.kde.org/show_bug.cgi?id=438839) (Oxalica F., Plasma 5.23.3)

В сеансе Wayland [наведение курсора мыши на виджет «Цифровые часы» для показа всплывающей подсказки больше никогда не приводит к зависанию Plasma](https://bugs.kde.org/show_bug.cgi?id=422072) (Marco Martin, Plasma 5.23.3)

В сеансе Wayland [анимация отображения/скрытия панели (при включённом режиме «Автоматически скрывать») теперь работает](https://bugs.kde.org/show_bug.cgi?id=443711) (Влад Загородний, Plasma 5.23.3)

В сеансе Wayland [заработала вставка произвольного содержимого буфера обмена в файл](https://invent.kde.org/frameworks/kio/-/merge_requests/619) (Méven Car, Frameworks 5.88)

## Исправления ошибок

Комментирование снимков экрана с прозрачными областями в Spectacle [больше не приводит к замене прозрачности на сплошной белый цвет](https://bugs.kde.org/show_bug.cgi?id=432753) (Julius Zint, Spectacle 21.12)

Виджет «Сетевые подключения» [теперь позволяет успешно подключиться к серверу OpenVPN с сертификатом `.p12`, защищённым парольной фразой](https://bugs.kde.org/show_bug.cgi?id=444882) (Jan Grulich, Plasma 5.23.3)

Исправлен случай, [когда запуск Системного монитора мог привести к вылету процесса `ksgrd_network_helper`](https://bugs.kde.org/show_bug.cgi?id=444921) (Arjen Hiemstra, Plasma 5.23.3)

Сценарий «Свернуть всё» диспетчера окон KWin [теперь запоминает, какое окно было активным, и гарантирует, что это окно окажется наверху при восстановлении всех свёрнутых окон](https://invent.kde.org/plasma/kwin/-/merge_requests/1603) (Влад Загородний, Plasma 5.23.3)

Исправлена [одна из наиболее распространённых причин сбоев в Параметрах системы, приводившая к сбоям при быстром переходе между страницами](https://bugs.kde.org/show_bug.cgi?id=429027) (Harald Sitter, Frameworks 5.88)

При использовании стороннего набора значков [значки, запрашиваемые приложением и недоступные в активном наборе, теперь заимствуются из указанного резервного набора, а не просто отсутствуют](https://invent.kde.org/frameworks/kiconthemes/-/merge_requests/45) (Carl Schwan, Frameworks 5.88)

Слишком длинные подписи на страницах Параметров системы в виде сетки [теперь сокращаются многоточием, а не вылезают за границы](https://bugs.kde.org/show_bug.cgi?id=444707) (Nate Graham, Frameworks 5.88)

## Улучшения пользовательского интерфейса

Древний веб-сайт системы учёта ошибок KDE — <https://bugs.kde.org> — недавно был визуально обновлён, и теперь он куда больше радует глаз при использовании! Спасибо Debarpan Debnath за работу над этим!

<!-- Вставить снимок -->

Функция «внешняя рамка фокуса» из Plasma 5.24 [была перенесена в Plasma 5.23](https://invent.kde.org/plasma/breeze/-/merge_requests/160), поскольку она решает ряд ошибок и проблем, связанных с фокусировкой элементов управления, и на данный момент доказала свою стабильность (Noah Davis, Plasma 5.23.3)

При отключении экрана окна [стали запоминать, на каком экране они были, и при его повторном подключении возвращаются туда](https://invent.kde.org/plasma/kwin/-/merge_requests/1588#note_331696). Это должно исправить [огромный класс проблем](https://bugs.kde.org/show_bug.cgi?id=296673) связанных с [многомониторными](https://bugs.kde.org/show_bug.cgi?id=378896) [конфигурациями](https://bugs.kde.org/show_bug.cgi?id=412703)! (Влад Загородний, Plasma 5.24)

У критических уведомлений [теперь есть тонкая оранжевая полоска сбоку, призванная сделать их заметнее на фоне беспорядка](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1113) (Nate Graham, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2021/11/screenshot_20211014_102213.png)

Странное поведение щелчка средней кнопкой мыши по панели для создания заметки [было отключено путём удаления соответствующих записей из файлов конфигурации у пользователей, у которых оно по какой-то причине всё ещё там было](https://bugs.kde.org/show_bug.cgi?id=425852) (Nate Graham, Plasma 5.24)

В наборе значков Breeze [появилось множество значков папок с различными наложенными на них сколько-нибудь распространёнными значками и эмблемами](https://invent.kde.org/frameworks/breeze-icons/-/merge_requests/154) (Andreas Kainz, Frameworks 5.88):

![1](https://pointieststick.files.wordpress.com/2021/11/custom-breeze-folder-icons.png)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Максим Маршев](https://t.me/msmarshev)  
_Источник:_ https://pointieststick.com/2021/11/05/this-week-in-kde-more-stability-more-features-prettier-bug-tracker/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: icon theme → набор значков -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: venerable → древний -->
