# Kdenlive 21.12 is out

The last and most exciting release of Kdenlive this year is out and brings long awaited features like Multicam Editing and Slip trimming mode, all of which drastically improve your editing workflow.

This version also comes with a new deep-learning based tracking algorithm, an auto-magical noise reduction filter and support for multiple Project Bins. Speaking of Bins, now you can import your video footage or audio recording folders while automatically ignoring any sub-folder structures created by some devices, such as the Sony XDCam, Panasonic P2, Canon camcorders or Zoom audio recorders.

Apart from all these nifty features, the new Kdenlive adds a vast array of enhancements and fixes across many aspects, most notably to same-track transitions and the motion tracker. We also added an option to loop the selected clip in the timeline, and, by pressing Esc, you can now default back to selection mode when using the editing tools.

In other news, we have switched our user manual to a new architecture called Sphinx. Sphinx provides a better and more user-friendly experience while making it easier to maintain. During this move, we’ve started updating old sections, while adding new ones. It is a great opportunity for the community to contribute, check it out [here](https://docs.kdenlive.org)! The macOS (Intel) version is now considered stable and ready for prime time. We’ve also updated the license of our code base to GPLv3.

## Advanced Trimming

Kdenlive 21.12 comes with the Slip trimming mode which shifts, in a single operation, the IN and OUT points of a clip forward or backward by the same number of frames, while keeping the original duration and without affecting adjacent clips.

Select a clip in the timeline and go to menu *Tool* → *Slip tool.* You may then slip the clip in the timeline to dragging it.

![0](https://kdenlive.org/wp-content/uploads/2021/12/slip.gif)

## Multicam Editing

Add your clips in different tracks, but at the same position in the timeline and activate the multicam tool by going to menu Tool → Multicam tool. You may trim the clips in the desired track while the timeline is playing by pressing their corresponding numbers (for track V1, press key 1; for track V2 press key 2, etc…) or simply select the desired track in the project monitor by clicking on it with the mouse.

<https://kdenlive.org/wp-content/uploads/2021/12/multicam.mp4>

## Motion Tracking

### DaSiamRPN Tracker

The DaSiamRPN visual tracking algorithm relies on deep-learning models to provide extremely accurate results. Do note that this feature requires AI models to be downloaded first, see instructions [here](https://docs.kdenlive.org/en/effects_and_compositions/effect_groups/alpha_manipulation/motion_tracker.html?highlight=motion%20tracker#id2).

![2](https://kdenlive.org/wp-content/uploads/2021/12/tracking-ui.png)

### Object Obscure

This version comes with new object obscure methods like Pixelate and Opaque Fill.

<https://kdenlive.org/wp-content/uploads/2021/12/object-obscure.mp4>

## Multiple Bins

This new feature allows creating various bins from folders. You may close the extra Bins either by pressing Ctrl+w or from the hamburger menu of the Bin you want to close.

![4](https://kdenlive.org/wp-content/uploads/2021/12/multibin.gif)

_Автор:_ Команда Kdenlive  
_Перевод:_ [Илья Бизяев](https://ilyabiz.com)  
_Полный список изменений:_ https://kdenlive.org/en/2021/12/kdenlive-21-12-is-out/  
